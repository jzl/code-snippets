/*
 * =====================================================================================
 *
 *       Filename:  kmp.c
 *
 *    Description:  a C implementation of KMP string match algorithm
 *
 *        Version:  1.0
 *        Created:  02/18/2014 11:37:30 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Ji.Zhilong (), zhilongji@gmail.com
 *   Organization:  SJTU
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void fetal_error(char *msg)
{
    fprintf(stderr, "Fetal Error: %s\n", msg);
    abort();
}

int
kmp(char *p, char *s, int l)
{
    int *f;
    int i, t;

    f = malloc(sizeof(int)*l);
    if (NULL == f) {
        fetal_error("no enough memory!");
    }

    t = 0;
    f[0] = 0;

    for (i = 1; i < l; i++) {  
        /* set up the failure table, f[i] means if s[i] != p[i], 
         * what's the longest common substring of the prefix of p, and the postfix of s.
         * surely f[0] = 0, f[1] = 1 if s[1] == p[0], 
         * if f[i] == t, then f[i+1] = t+1 only if s[i+1] == p[t+1],
         * else try a shorter postfix */
        while (t > 0 && p[i] != p[t]) t = f[t-1]; 
        /* t is the length of prefix, t -1 is the index of the last char of the prefix in the original string */
        if (p[i] == p[t]) 
            t++;
        f[i] = t;
    }

    for (i = 0; i < l; i++)
        printf("%d ", i);
    printf("\n");
    for (i = 0; i < l; i++)
        printf("%d ", f[i]);
    printf("\n");

    t = 0;
    for (i = 0; s[i]; i++) {
        while (t > 0 && s[i] != p[t]) t = f[t-1];
        if (s[i] == p[t]) t++;
        if (t == l) return 1;
    }

    return 0;
}

int
main(int argc, char *argv[])
{
    int l;
    if (argc >= 3) {
        l = strlen(argv[1]);
        if (kmp(argv[1], argv[2], l)) {
            printf("%s is in %s\n", argv[1], argv[2]);
        } else {
            printf("%s is not in %s\n", argv[1], argv[2]);
        }
    } else {
        printf("wrong arguments\n");
        return -1;
    }

    return 0;
}
