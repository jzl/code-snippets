/*
 * Deleting the minimum
 */

public void deleteMin()
{
	root = deleteMin(root);
}

private Node deleteMin(Node x)
{
	if (x.left == null) return x.right;
	x.left = deleteMin(x.left);
	x.count = 1 + size(x.left) + size(x.right);
	return x;
}


/*
 * Hibbard deletion: 
 * delete the node with the specified key
 * replace it with the minimum in its right child
 */

public void delete(Key key)
{ root = delete(root, key); }

private Node delete(Node x, Key key)
{
	if (x == null) return null;
	int cmp = key.compareTo(x.key);
	if (cmp < 0) x.left = delete(x.left, key);
	else if (cmp > 0) x.right = delete(x.right, key);
	else {
		if (x.right == null) return x.left;

		Node t = x;
		x = min(t.right);
		x.right = deleteMin(t.right);
		x.left = t.left;
	}
	x.count = size(x.left) + size(x.right) + 1;
	return x;
}
