/*
 * rotate left
 */
private Node rotateLeft(Node h)
{
	assert isRed(h.right);
	Node x = h.right;
	h.right = x.left;
	x.left = h;
	x.color = h.color;
	h.color = RED;
	return x;
}

/*
 * rotate right
 */
private Node rotateRight(Node h)
{
	assert isRed(h.left);
	Node x = h.left;
	h.lrft = x.right;
	x.right = h;
	x.color = h.color;
	h.color = RED;
	return x;
}

/*
 * flip colors
 */

private void flipColors(Node h)
{
	assert !isRed(h);
	assert isRed(h.left);
	assert isRed(h.right);
	h.color = RED;
	h.left.color = BLACK;
	h.right.color = BLACK;
}

/*
 * put a new node in a red-black tree
 */

private Node put(Node h, Key key, Value val)
{
	if (h == null) return new Node(key, val, RED);
	int cmp = key.compareTo(h.key);
	if      (cmp < 0) h.left = put(h.left, key, val);
	else if (cmp > 0) h.right = put(h.right, key, val);
	else if (cmp == 0) h.val = val;

	if (isRed(h.right) && !isRed(h.left)) h = rotateLeft(h); /* lean left */
	if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h); /* blance 4-node */
	if (isRed(h.left) && isRed(h.right))  flipColors(h); /* split 4-node */

	return h;
}
