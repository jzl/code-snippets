/*
 * build epsilon transition diagraph for a NFA
 * character transition is not included in this method
 * taken from https://class.coursera.org/algs4partII-001/lecture/48
 */

private Diagraph buildEpsilonTransitionDiagraph() {
	Diagraph G = new Diagraph(M+1);
	Stack<Integer> ops = new Stack<Integer>();
	for (int i = 0; i < M; i++) {
		int lp = i;     // left parenthesis

		if (re[i] == '(' || re[i] == '|') ops.push(i);

		else if (re[i] == ')') {
			int or = ops.pop();
			if (re[or] == '|') {
				lp = ops.pop();   // '|' must follow a '('
				G.addEdge(lp, or+1);
				G.addEdge(or, i);  // remember , here i is ')'
			}
			else lp = or;        // no '|' followint the last '('
		}

		if (i < M-1 && re[i+1] == '*') {
			G.addEdge(lp, i+1);
			G.addEdge(i+1, lp);
		}

		if (re[i] == '(' || re[i] == '*' || re[i] == ')')
			G.addEdge(i, i+1);
	}

	return G;
}
