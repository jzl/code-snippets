public class QuickUnionUF {
	private int []id;
	private int []size;
	private int count;
	public QuickUnionUF(int n) {
		id = new int[n];
		size = new int[n];
		for(int i = 0; i < n; i ++) {
			id[i] = i;
			size[i] = 1;
		}
	}

	private int root(int i) {
		while(i != id[i]) {
			id[i] = id[id[i]];
			i = id[i];
		}
		return i;
	}
	
	public boolean connected(int p , int q) {
		return root(p) == root(q);
	}

	public void union(int p, int q) {
		int i = root(p);
		int j = root(q);
		if(i == j) return;
		if(size[i] > size[j]) {
			id[j] = i;
			size[i] += size[j];
		} else if(size[i] < size[j]){
			id[i] = j;
			size[j] += size[i];
		}
		count--;
	}

	public int count() {
		return count;
	}

	public static void main(String[] args) {
		int N = StdIn.readInt();
		QuickUnionUF uf = new QuickUnionUF(N);
		while (!StdIn.isEmpty())
		{
			int p = StdIn.readInt();
			int q = StdIn.readInt();
			if(!uf.connected(p, q))
			{
				uf.union(p,q);
				StdOut.println(p + " " + q);
			}
		}
	}
}
