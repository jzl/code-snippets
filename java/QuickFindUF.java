public class QuickFindUF {
	private int id[];
	int count;
	public QuickFindUF(int n) {
		id = new int[n];
		for(int i = 0; i < n; i ++) {
			id[i] = i;
		}
		count = n;
	}

	void union(int p , int q) {
		if(connected(p, q)) {
			return;
		}

		int pid = id[p];
		int qid = id[q];
		for(int i = 0; i < id.length; i++) {
			if(id[i] == qid) {
				id[i] = pid;
			}
		}
		count--;
	}

	boolean connected(int p, int q) {
		if(p >= id.length || q >= id.length) {
			throw new IllegalArgumentException();
		}
		return id[p] == id[q];
	}

	int find(int p) {
		if(p >= id.length) {
			throw new IllegalArgumentException();
		}
		return id[p];
	}

	int count() {
		return count;
	}

	public static void main(String[] args) {
		int N = StdIn.readInt();
		QuickFindUF uf = new QuickFindUF(N);
		while (!StdIn.isEmpty())
		{
			int p = StdIn.readInt();
			int q = StdIn.readInt();
			if(!uf.connected(p, q))
			{
				uf.union(p,q);
				StdOut.println(p + " " + q);
			}
		}
	}
}
