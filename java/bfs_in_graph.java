/*
 * Breadth First Search in Graph
 * taken from: https://class.coursera.org/algs4partII-001/lecture/4
 */

public class BreadthFirstPaths
{
	private boolean[] marked;
	private boolean[] edgeTo[];
	private final int s;

	private void bfs(Graph G, int s)
	{
		Queue<Integer> q = new Queue<Integer>();
		q.enqueue(s);
		marked[s] = true;
		while(!q.isEmpty())
		{
			int v = q.dequeue();
			for(int w : G.adj(v))
			{
				if(!ismarked[w])
				{
					q.enqueue(w);
					marked[w] = true;
					edgeTo[w] = v;
				}
			}
		}
	}
}
